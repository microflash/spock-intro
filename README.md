# Introduction to Spock

Simple examples to get started with Spock

## Instructions

1. Clone this repo
2. Import as Maven project
3. Edit Database driver for your database in `pom.xml`
4. Run `mvn clean package install`
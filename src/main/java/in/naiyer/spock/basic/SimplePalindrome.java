package in.naiyer.spock.basic;

public class SimplePalindrome {

  public boolean isPalindrome(final String input) {
    int frwrd = 0;
    int bkwrd = input.length() - 1;
    String str = input.toLowerCase();
    while (bkwrd > frwrd) {
      if (str.charAt(frwrd) != str.charAt(bkwrd)) {
        return false;
      }
      frwrd++;
      bkwrd--;
    }
    return true;
  }

  public boolean isPalindrome(final int input) {
    return isPalindrome(String.valueOf(Math.abs(input)));
  }
}
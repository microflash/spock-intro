package in.naiyer.spock.basic;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

public class User {
  private String username;
  private List<User> following;
  private List<Post> posts;
  private Instant registered;

  public User(String username) {
    this.username = username;
    this.following = new ArrayList<>();
    this.posts = new ArrayList<>();
    this.registered = Instant.now();
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public void setFollowing(List<User> following) {
    this.following = following;
  }

  public void setPosts(List<Post> posts) {
    this.posts = posts;
  }

  public void setRegistered(Instant registered) {
    this.registered = registered;
  }

  public String getUsername() {
    return username;
  }

  public List<User> getFollowing() {
    return following;
  }

  public List<Post> getPosts() {
    return posts;
  }

  public Instant getRegistered() {
    return registered;
  }
}

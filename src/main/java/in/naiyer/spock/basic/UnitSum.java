package in.naiyer.spock.basic;

public class UnitSum {
  public int getUnitSum(final long number) {
    long sum = Math.abs(number);
    int itr;

    while (sum > 9) {
      itr = 0;
      while (sum > 0) {
        itr += sum % 10;
        sum /= 10;
      }
      sum = itr;
    }
    return (int) sum;
  }
}

package in.naiyer.spock.basic

import spock.lang.Specification

import java.time.Instant

class GroupedAssertionsSpec extends Specification {

    def "Multiple Assertions on an Object"() {
        given:
        def user = new User("Emma")

        expect:
        with(user) {
            username == "Emma"
            following.isEmpty()
            posts.isEmpty()
            registered instanceof Instant
        }
    }
}

package in.naiyer.spock.basic

import spock.lang.Specification

class ExpectExceptionSpec extends Specification {

    def "Expect StringIndexOutOfBoundsException"() {
        setup:
        def str = "Grace is Eternal"

        when:
        str.getAt(43)

        then:
        thrown(StringIndexOutOfBoundsException)
    }
}

package in.naiyer.spock.basic

import spock.lang.Specification

class InterrogateExceptionSpec extends Specification {

    def "Interrogate ArithmeticException"() {
        given:
        def num = 10
        def zero = 0

        when:
        num / zero

        then:
        def e = thrown(ArithmeticException)
        e.message == "Division by zero"
    }
}

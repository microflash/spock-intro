package in.naiyer.spock.basic

import spock.lang.Shared
import spock.lang.Specification

import java.sql.Connection
import java.sql.DriverManager
import java.sql.ResultSet
import java.sql.Statement

class DatabaseSpec extends Specification {

    private final url = "jdbc:mariadb://localhost:3306/spring"
    private final username = "erin"
    private final password = "richards"

    @Shared Connection connection

    def setup() {
        connection = DriverManager.getConnection(url, username, password)
    }

    def "DB test"() {
        given:
        Statement statement = connection.createStatement()
        final query = "select * from book where book_id = 1"

        when:
        ResultSet resultSet = statement.executeQuery(query)

        then:
        resultSet.getMetaData().getColumnCount() > 1

        cleanup:
        resultSet.close()
    }

    def cleanup() {
        connection.close()
    }
}

package in.naiyer.spock.basic

import spock.lang.Specification
import spock.lang.Unroll

class UnrollingSpec extends Specification  {

    @Unroll("is #str a palindrome? #answer")
    def "Test for Palindrome" () {
        given:
        def palindrome = new SimplePalindrome()

        expect:
        palindrome.isPalindrome(str) == expected

        where:
        str       |   expected
        "BOB"     |   true
        "vIv"     |   true
        "A"       |   true
        "Holly"   |   false

        answer = expected ? "YES" : "NO"
    }
}

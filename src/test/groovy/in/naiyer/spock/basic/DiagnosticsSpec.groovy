package in.naiyer.spock.basic

import spock.lang.Specification

class DiagnosticsSpec extends Specification {

    def "check string length"() {
        given:
        def str = new StringBuilder()

        expect:
        str.append("Hello").append("-World").replaceAll("-", ", ").toString() == "Hello World"
    }
}

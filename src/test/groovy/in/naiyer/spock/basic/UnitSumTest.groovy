package in.naiyer.spock.basic;

import spock.lang.Specification

class UnitSumTest extends Specification {
    def "Test summation to a unit" () {
        given:
        def sum = new UnitSum()

        expect:
        sum.getUnitSum(input) == output

        where:
        input   |   output
        1234    |   1
        15678   |   9
        -35567  |   8
        0       |   0
    }
}
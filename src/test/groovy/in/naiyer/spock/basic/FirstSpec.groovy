package in.naiyer.spock.basic

import spock.lang.Specification

class FirstSpec extends Specification {

    def "length of Carmaker name"() {
        expect:
        "BMW".length() == 3
    }
}

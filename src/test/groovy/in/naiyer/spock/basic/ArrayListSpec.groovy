package in.naiyer.spock.basic

import spock.lang.Shared
import spock.lang.Specification

class ArrayListSpec extends Specification {
    @Shared list = new ArrayList<String>()

    def "check if list is clear"() {
        list.add("Allspark")

        when:
        list.clear()

        then:
        list.size() == 0

    }
}

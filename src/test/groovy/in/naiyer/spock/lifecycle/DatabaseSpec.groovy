package in.naiyer.spock.lifecycle

import spock.lang.Shared
import spock.lang.Specification

import java.sql.Connection
import java.sql.DriverManager

abstract class DatabaseSpec extends Specification {

    private final url = "jdbc:mariadb://localhost:3306/spring"
    private final username = "erin"
    private final password = "richards"

    @Shared Connection connection

    def setup() {
        connection = DriverManager.getConnection(url, username, password)
    }

    def cleanup() {
        connection.close()
    }
}

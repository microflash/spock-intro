package in.naiyer.spock.lifecycle

class SubSpec extends SuperSpec {
    def setupSpec() {
        println("sub setupSpec")
    }

    def cleanupSpec() {
        println("sub cleanupSpec")
    }

    def setup() {
        println("sub setup")
    }

    def cleanup() {
        println("sub cleanup")
    }

    def "feature method 1"() {
        expect:
        println("feature method 1")
    }

    def "feature method 2"() {
        expect:
        println("feature method 2")
    }
}

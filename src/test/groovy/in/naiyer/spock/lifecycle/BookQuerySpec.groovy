package in.naiyer.spock.lifecycle

import java.sql.ResultSet
import java.sql.Statement

class BookQuerySpec extends DatabaseSpec {

    private static ResultSet resultSet

    def "DB test"() {
        given:
        Statement statement = connection.createStatement()
        final query = "select * from book where book_id = 1"

        when:
        resultSet = statement.executeQuery(query)

        then:
        resultSet.getMetaData().getColumnCount() > 1
    }

    def cleanup() {
        resultSet.close()
    }
}

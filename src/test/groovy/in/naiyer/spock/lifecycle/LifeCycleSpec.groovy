package in.naiyer.spock.lifecycle

import spock.lang.Specification

class LifeCycleSpec extends Specification {

    def setupSpec() {
        println("setupSpec")
    }

    def setup() {
        println("setup")
    }

    def "feature method 1"() {
        expect:
        println("feature method 1")
    }

    def "feature method 2"() {
        expect:
        println("feature method 2")
    }

    def cleanup() {
        println("cleanup")
    }

    def cleanupSpec() {
        println("cleanupSpec")
    }
}
